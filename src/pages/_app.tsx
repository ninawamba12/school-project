import React from 'react'
import Layout from './components/Layout'
import '../styles/globals.css'

interface MyAppProps{
  Component: React.ComponentType<any>;
  pageProps: any;
}
const MyApp: React.FC<MyAppProps> = ({ Component, pageProps }) => {
  return (
      <Layout>
        <Component {...pageProps} />
      </Layout>
  )
}

export default MyApp