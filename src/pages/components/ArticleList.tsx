import React from 'react'
import ArticleItem from '@/pages/components/ArticleItem'
import articleStyles from '@/styles/Article.module.css'

interface ArticleListProps {
    articles: {
      id: string;
      title: string;
      excerpt: string;
    }[];
}
const ArticleList: React.FC<ArticleListProps>  = ({ articles }) => {
    return (
        <div className={articleStyles.grid}>
             {articles.map((article) => (
             <ArticleItem key={article.id} article={article} />
            ))}
        </div>
    );
};

export default ArticleList;