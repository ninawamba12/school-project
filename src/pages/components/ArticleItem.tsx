import React from 'react'
import Link from 'next/link'
import articleStyles from '@/styles/Article.module.css'

interface ArticleItemProps {
  article: {
    id: string;
    title: string;
    excerpt: string;
  };
}

const ArticleItem: React.FC<ArticleItemProps> = ({ article }) => {
    return (
      <Link legacyBehavior href={`/article/${article.id}`} passHref>
        <a className={articleStyles.card}>
          <h3>{article.title} &rarr;</h3>
          <p>{article.excerpt}</p>
        </a>
      </Link>
    );
  };

export default ArticleItem;