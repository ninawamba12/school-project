import React from "react";
import Nav from "./Nav";
import Meta from '@/pages/components/Meta'
import Header from './Header'
import styles from "@/styles/Layout.module.css";

interface LayoutProps {
  children: React.ReactNode;
}

const Layout: React.FC<LayoutProps> = ({ children }) => {
  return (
    <>
      <Meta />
      <Nav />
      <div className={styles.container}>
        <main className={styles.main}>
            <Header />
          {children}
        </main>
      </div>
    </>
  );
};

export default Layout;
