import {articles} from '@/pages/data'

export default function handler(req, res) {
    res.status(200).json(articles)
}