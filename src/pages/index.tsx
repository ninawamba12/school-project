import React from 'react';
import { server } from '@/pages/config'
import ArticleList from '@/pages/components/ArticleList'

interface HomeProps{
  articles: any[];
}

const Home: React.FC<HomeProps> = ({ articles }) => {
  return (
    
    <div>
      <ArticleList articles={ articles } />
    </div>

);
}; 

export default Home;

export const getStaticProps = async () => {
  const res = await fetch(`https://jsonplaceholder.typicode.com/posts?_limit=6`)
  const articles = await res.json()

  return {
    props: {
      articles,
    },
  };
};